.PHONY: build watch

watch:
	esbuild ./assets/js/main.js ./assets/css/style.css --bundle --outdir=build --watch

build:
	esbuild ./assets/js/main.js ./assets/css/style.css --bundle --minify --outdir=build

run:
	cd server && go run .

linux:
	cd server && GOOS=linux GOARCH=amd64 go build -o malparlats .