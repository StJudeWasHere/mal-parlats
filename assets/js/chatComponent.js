function ChatComponent(chatElement) {
	const CHAT_ME = "me";
	const CHAT_ENEMY = "enemy";
	const CHAT_SYSTEM = "system";
	const CHAT_ALERT = "alert";

	function add(className, text) {
		let el = document.createElement("div");
		el.classList.add(className);
		el.innerHTML = text;
		chatElement.append(el);
		window.scrollTo(0, document.body.scrollHeight);
	}

	this.addPlayerChat = function(text) {
		add(CHAT_ME, text);
	}

	this.addEnemyChat = function(text) {
		add(CHAT_ENEMY, text);
	}

	this.addSystemChat = function(text) {
		add(CHAT_SYSTEM, text);
	}

	this.addAlertChat = function(text) {
		add(CHAT_ALERT, text);
	}

	this.reset = function() {
		chatElement.innerHTML = "";
	}
}

export default ChatComponent;