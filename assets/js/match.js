function Match(player, chat, scoreComponent, playerComponent) {
	const UI_MESSAGES = {
		playerWins: "Has guanyat! El teu oponent no ha sabut contestar bé a tots els teus insults.",
		enemyWins: "Has perdut! No has sabut contestar bé als insults del teu oponent.",
		tie: "Sou molt burros els dos. Torna a jugar un altre cop, ara has après tots els insults i respostes del teu oponent i tens més recursos per guanyar la propera partida.",
		goodDefense: "Bona resposta, ara ets tu el que pot insultar.",
		goodAttack: "Bon insult, el teu oponent no ha sabut contestar. Et torna a tocar.",
		startAttack: "Comença la ronda d'insults. Et toca insultar primer.",
		startDefend: "El teu oponent comença la ronda d'insults.",
		enemyDisconnect: "El teu oponent s'ha desconnectat"
	};

	let currentAttack;
	let attackCount;
	let playerTurn;
	let playerScore;
	let enemyScore;
	let conn;
	let ended;
	let maxAttacks;

	this.setConn = function(websocket) {
		conn = websocket;
	}

	this.init = function(attack, ma) {
		maxAttacks = ma

		ended = false;
		attackCount = 0;
		playerScore = 0;
		enemyScore = 0;
		playerTurn = attack;

		scoreComponent.update(playerScore, enemyScore);
		scoreComponent.setTotalAttacks(maxAttacks);
		playerComponent.setAttackTurn(attack);
		playerComponent.showButton(attack);
		playerComponent.showPlayAgain(false);

		chat.reset();

		playerComponent.reset(player.getAttacks(), player.getDefenses());
		chat.addSystemChat(attack ? UI_MESSAGES.startAttack : UI_MESSAGES.startDefend);	
	}

	this.setCurrentAttack = function(attack) {
		if (playerTurn) {
			conn.send(JSON.stringify({Name: "attack", Data: attack}));
			chat.addPlayerChat(attack)
		} else {
			chat.addEnemyChat(attack)
		}

		currentAttack = attack;
		attackCount++;
		playerTurn = !playerTurn;
		playerComponent.setAttackTurn(false);
		playerComponent.showButton(playerTurn);

		scoreComponent.setTotalAttacks(maxAttacks - attackCount);
		if (playerTurn && player.addAttack(attack)) {
			playerComponent.addAttack(attack);
		}
	}

	this.setDefense = function(defense) {
		if (playerTurn) {
			conn.send(JSON.stringify({Name: "defense", Data: defense}));
			playerComponent.showButton(false);
			chat.addPlayerChat(defense);
		} else {
			chat.addEnemyChat(defense);
		}
	}

	this.isInvalidDefense = function() {
		playerTurn ? enemyScore++ : playerScore++;
		scoreComponent.update(playerScore, enemyScore);

		if (ended == true) {
			return;
		}

		if (!playerTurn) {
			chat.addSystemChat(UI_MESSAGES.goodAttack);
		}

		playerTurn = !playerTurn;
		playerComponent.setAttackTurn(playerTurn);
		playerComponent.showButton(playerTurn);
	}

	this.isValidDefense = function() {
		if (ended) {
			return;
		}

		if (playerTurn) {
			chat.addSystemChat(UI_MESSAGES.goodDefense);
			playerComponent.showButton(playerTurn);
			playerComponent.setAttackTurn(true);
			playerComponent.showButton(true);
		}
	}

	this.enemyDisconnect = function() {
		chat.addAlertChat(UI_MESSAGES.enemyDisconnect);
		playerComponent.showButton(false);
		playerComponent.showPlayAgain(true);
	}

	this.endGame = function(data) {
		enemyScore = data.OponentScore;
		playerScore = data.PlayerScore;

		scoreComponent.update(playerScore, enemyScore);

		if (data.Data == "tie") {
			chat.addSystemChat(UI_MESSAGES.tie);
		} else if (data.Data == "win") {
			chat.addSystemChat(UI_MESSAGES.playerWins);
		} else if (data.Data == "loose") {
			chat.addSystemChat(UI_MESSAGES.enemyWins);
		}

		ended = true

		playerComponent.showButton(false);
		playerComponent.showPlayAgain(true);
	}
}

export default Match;