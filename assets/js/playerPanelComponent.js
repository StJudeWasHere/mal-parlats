function PlayerPanelComponent(attacksElement, defensesElement, panelButton, playAgainButton, mainPanel, spinner) {
	const UI_MESSAGES = {
		attack: "Selecciona un insult",
		defense: "Selecciona una resposta",
		back: "Tornar al chat",
	};

	let playerAttackTurn;
	let attackCallback;
	let defenseCallback;

	panelButton.addEventListener("click", function() {
		let element = playerAttackTurn ? attacksElement : defensesElement;
		if (element.style.display == "block") {
			element.style.display = "none";
			mainPanel.style.display = "block";
			panelButton.innerHTML = playerAttackTurn ? UI_MESSAGES.attack : UI_MESSAGES.defense;
			window.scrollTo(0, document.body.scrollHeight);
		} else {
			element.style.display = "block";	
			mainPanel.style.display = "none";
			panelButton.innerHTML = UI_MESSAGES.back;
			window.scrollTo(0, 0);
		}
	});

	function checkResolve() {
		if (playerAttackTurn) {
			attacksElement.style.display = "none";
			attackCallback(this.innerHTML);
			this.remove();
		} else {
			defensesElement.style.display = "none";
			defenseCallback(this.innerHTML);
		}
		mainPanel.style.display = "block";
		window.scrollTo(0, document.body.scrollHeight);
	}

	function addElement(text, element) {
		let responseElement = document.createElement("span");
		responseElement.innerHTML = text;
		responseElement.addEventListener('click', checkResolve)
		element.appendChild(responseElement);
	}

	this.setAttackCallback = function(callback) {
		attackCallback = callback;
	}

	this.setDefenseCallback = function(callback) {
		defenseCallback = callback;
	}

	this.addAttack = function(attack) {
		addElement(attack, attacksElement);
	}

	this.addDefense = function(defense) {
		addElement(defense, defensesElement);
	}

	this.reset = function(attacks, defenses) {
		attacks = attacks || [];
		defenses = defenses || [];

		attacksElement.innerHTML = "";
		defensesElement.innerHTML = "";

		for (i = 0; i<attacks.length; i++) {
			this.addAttack(attacks[i]);
		}
		for (i = 0; i<defenses.length; i++) {
			this.addDefense(defenses[i]);
		}
	}

	this.showButton = function(show) {
		panelButton.innerHTML = playerAttackTurn ?  UI_MESSAGES.attack : UI_MESSAGES.defense;
		if (show == true) {
			panelButton.style.visibility = "visible";
			spinner.style.display = "none";
		} else {
			panelButton.style.visibility = "hidden";
			spinner.style.display = "block";
		}
	}

	this.showPlayAgain = function(show) {
		spinner.style.display = "none";
		playAgainButton.style.visibility = show ? "visible" : "hidden";
	}

	this.setAttackTurn = function(attackTurn) {
		playerAttackTurn = attackTurn;
	}
}

export default PlayerPanelComponent;