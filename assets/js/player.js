function Player() {
	let attacks = [];
	let defenses = [];

	this.getAttacks = function() {
		return attacks;
	}

	this.getDefenses = function() {
		return defenses;
	}

	this.addAttack = function(attack) {
		if (!attacks.includes(attack)) {
			attacks.push(attack);
			return true;
		}
		
		return false;
	}

	this.addDefense = function(defense) {
		if (!defenses.includes(defense)) {
			defenses.push(defense);
			return true;
		}
		
		return false;
	}
}

export default Player;