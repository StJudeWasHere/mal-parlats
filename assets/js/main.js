import ScoreComponent from "./scoreComponent.js";
import ChatComponent from "./chatComponent.js";
import Player from "./player.js";
import PlayerPanelComponent from "./playerPanelComponent.js";
import Match from "./match.js";

document.addEventListener('DOMContentLoaded', () => {
	if (!window["WebSocket"]) {
		var item = document.createElement("div");
		item.innerHTML = "<b>El teu navegador no suporta WebSockets.</b>";
		document.body.appendChild(item)
		return
	}

	const BOT_TIMEOUT = 5000;
	const introElement = document.getElementById("intro");

	const playAgainButton = document.getElementById("play-again");
	const connectingElement = document.getElementById("connecting");
	const appElement = document.getElementById("app");
	const aliasElement = document.getElementById("alias");
	const botContainerElement = document.getElementById("bot-play-container");
	const botButtonElement = document.getElementById("bot-play");

	let player = new Player();
	let chat = new ChatComponent(document.getElementById("chat"));
	let scoreComponent = new ScoreComponent(
		document.getElementById("my-fails"),
		document.getElementById("enemy-fails"), 
		document.getElementById("total-attacks"), 
		document.getElementById("player-alias"), 
		document.getElementById("enemy-alias")
	);
	let playerComponent = new PlayerPanelComponent(
		document.getElementById("responses"),
		document.getElementById("defenses"),
		document.getElementById("chat-panel-button"),
		playAgainButton,
		document.getElementById("main-panel"),
		document.getElementById("spinner")
	);

	let match = new Match(player, chat, scoreComponent, playerComponent);

	playerComponent.setAttackCallback(function(attack) { match.setCurrentAttack(attack) });
	playerComponent.setDefenseCallback(function(defense) { match.setDefense(defense) });

	let conn;
	let alias;

	introElement.style.display = "block";

	document.getElementById("start").addEventListener("click", function(e) {
		alias = aliasElement.value;
		if (alias == "") {
			aliasElement.focus();
			return;
		}

		conn = new WebSocket("wss://"+ document.location.host + "/ws")
		// conn = new WebSocket("ws://127.0.0.1:9000/ws")
		scoreComponent.setPlayerAlias(alias);

		conn.onclose = function (evt) {
			connectingElement.style.display = "none";
			botContainerElement.style.display = "none";
			appElement.style.display = "none";

			var item = document.createElement("div");
			item.innerHTML = "<b>Connexió tancada. <a href=\"/\">Torna a connectar-te</a>.</b>";
			document.body.appendChild(item);
		};

		conn.onmessage = function (evt) {
			let data = JSON.parse(evt.data);
			switch (data.Name) {
			case "start":
				scoreComponent.setEnemyAlias(data.OponentAlias);
				for (i = 0; i< data.Attacks.length; i++) {
					player.addAttack(data.Attacks[i])
				}
				for (i=0; i< data.Defenses.length;i++) {
					player.addDefense(data.Defenses[i])
				}
				match.init(data.Turn, data.MaxAttacks);
				connectingElement.style.display = "none";
				botContainerElement.style.display = "none";
				appElement.style.display = "block";
				break;
			case "attack":
				match.setCurrentAttack(data.Data);
				break;
			case "defense":
				match.setDefense(data.Data);
				break;
			case "disconnect":
				match.enemyDisconnect();
				break;
			case "validation": 
				if (data.Valid) {
					match.isValidDefense();
				} else {
					match.isInvalidDefense();
				}
				break;
			case "end":
				match.endGame(data);
				break;
			}
		};

		conn.addEventListener('open', (event) => {
			match.setConn(conn)
			conn.send(JSON.stringify({Name: "connect", Data: alias}));
		});

		introElement.style.display = "none";
		showConnectPanel();
	});

	playAgainButton.addEventListener("click", function() {
		this.style.visibility = "hidden";
		chat.reset();
		conn.send(JSON.stringify({Name: "connect", Data: alias}));
		showConnectPanel();
	});

	function showConnectPanel() {
		appElement.style.display = "none";
		connectingElement.style.display = "block";
		setTimeout(function() {
			botContainerElement.style.display = "block";
			botButtonElement.addEventListener("click", function() {
				conn.send(JSON.stringify({Name: "playbot"}));
				botContainerElement.style.display = "none";
			});
		}, BOT_TIMEOUT);
	}
});
