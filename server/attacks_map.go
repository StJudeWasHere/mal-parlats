package main

type AttackDefense struct {
	Attack  string
	Defense string
}

var AttacksMap []AttackDefense = []AttackDefense{
	AttackDefense{
		Attack:  "Si fossis menys burro i més net, seguiries sense agradar-li a ta mare. 💔",
		Defense: "I ta mare diu que tinc la intel·ligència i l'elegància que li falta al seu fill!",
	},
	AttackDefense{
		Attack:  "Ets tan lleig que si volessis t'hauria confós amb un ratpenat. 🦇🦇🦇",
		Defense: "Si m'assemblés a un ratpenat, ens haurien pres per germans bessons. 👩‍👦‍👦 ",
	},
	AttackDefense{
		Attack:  "Em sorprèn saber que tens la intel·ligència d'un 🐵 mico, pensava que no en tenies gens ni mica.",
		Defense: "I això ho diu algú que no ha llegit un llibre en la vida.",
	},
	AttackDefense{
		Attack:  "Ets tan lleig que sembles un cap de porc amb orelles! 🐷",
		Defense: "Millor semblar un porc que el cul d'una truja!",
	},
	AttackDefense{
		Attack:  "De tan curt que ets, et perds fins i tot al supermercat.",
		Defense: "Per curt el teu cap, que no tens ni dos dits de front!",
	},
	AttackDefense{
		Attack:  "Ets pitjor que una inversió a fons perdut. 💸 ",
		Defense: "Les que estan perdudes són les teves neurones.",
	},
	AttackDefense{
		Attack:  "Tens la cara més dura que el genoll d'un ase.",
		Defense: "En tot cas d'un ruc català, ben fet. No com tu, mitja merda! 💩",
	},
	AttackDefense{
		Attack:  "Estic perdent el ⏱ temps amb tu, no ets digne de la meva atenció, ni de la de ningú.",
		Defense: "El teu dèficit d'atenció és una tara de naixement.",
	},
	AttackDefense{
		Attack:  "He vist 🥔🥔 patates arrugades més maques que tu!",
		Defense: "Calla, que sembla que baixes de l'hort, bleda assolellada. 🥬",
	},
	AttackDefense{
		Attack:  "Mala cagarada caigui del cel i t’esclafi, ensuma-llufes 💨",
		Defense: "Ves que et moqui la 👵 iaia, que volies ser merda 💩 i no arribes a pet",
	},
	AttackDefense{
		Attack:  "Animalot, que ets tan bèstia que si et sacsegen cauen garrofes.",
		Defense: "Quan parles sembles una 🐓 gallina, tanca el pico!",
	},
	AttackDefense{
		Attack:  "Innocent, que encara tens la intel·ligència per estrenar.",
		Defense: "Au va, que tu tens la closca tan dura que no saps ni quants dits tens a la mà. ✋",
	},
}

func isValidDefense(a string, d string) bool {
	for _, v := range AttacksMap {
		if v.Attack == a && v.Defense == d {
			return true
		}
	}

	return false
}
