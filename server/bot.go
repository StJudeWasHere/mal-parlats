package main

import (
	"math/rand"
	"time"
)

const (
	botAlias = "Bot"
)

type Bot struct {
	attacks        []string
	attackDefenses []AttackDefense
	match          *Match
	send           chan interface{}
	end            bool
	alias          string
}

func newBot() *Bot {
	b := Bot{send: make(chan interface{}), alias: botAlias}
	rand.Seed(time.Now().UnixNano())

	for len(b.attacks) < maxElements {
		r := rand.Intn(len(AttacksMap) - 1)
		if !contains(b.attacks, AttacksMap[r].Attack) {
			b.attackDefenses = append(b.attackDefenses, AttacksMap[r])
			b.attacks = append(b.attacks, AttacksMap[r].Attack)
		}
	}

	go b.sender()

	return &b
}

func (b *Bot) sender() {
	defer func() {
		close(b.send)
	}()

	for b.end == false {
		v := <-b.send
		switch v := v.(type) {
		case *BasicMessage:
			msg := BasicMessage(*v)
			switch msg.Name {
			case DisconnectName, EndTie, EndWin, EndLoose:
				b.end = true
			}
		case *DataMessage:
			msg := DataMessage(*v)
			if msg.Name != AttackName {
				continue
			}

			var d string
			for i := 0; i < len(b.attackDefenses); i++ {
				if b.attackDefenses[i].Attack == msg.Data {
					d = b.attackDefenses[i].Defense
					break
				}
			}

			if d == "" {
				r := rand.Intn(len(b.attackDefenses) - 1)
				d = b.attackDefenses[r].Defense
			}

			delay()
			b.match.play <- &PlayerMessage{player: b, message: &DataMessage{Name: DefenseName, Data: d}}
		case *ValidationMessage:
			if b.match.turn == b {
				r := rand.Intn(len(b.attacks) - 1)
				delay()
				b.match.play <- &PlayerMessage{player: b, message: &DataMessage{Name: AttackName, Data: b.attacks[r]}}
				b.attacks = append(b.attacks[:r], b.attacks[r+1:]...)
			}
		}
	}
}

func (b *Bot) sendOut(v interface{}) {
	b.send <- v
}

func (b *Bot) setMatch(m *Match) {
	b.match = m
}

func (b *Bot) getAttacks() []string {
	return b.attacks
}

func (b *Bot) getDefenses() []string {
	var ds []string
	for i := 0; i < len(b.attackDefenses); i++ {
		ds = append(ds, b.attackDefenses[i].Defense)
	}

	return ds
}

func (b *Bot) addAttack(a string) {
	return
}

func (b *Bot) addDefense(d string) {
	return
}

func (b *Bot) isReadyToPlay() bool {
	return true
}

func (b *Bot) getAlias() string {
	return b.alias
}

func delay() {
	r := rand.Intn(1500) + 1000
	time.Sleep(time.Duration(r) * time.Millisecond)
}
