package main

import (
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"time"
)

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

type Client struct {
	conn        *websocket.Conn
	send        chan interface{}
	attacks     []string
	defenses    []string
	match       *Match
	hub         *Hub
	readyToPlay bool
	alias       string
}

func (c *Client) read() {
	defer func() {
		close(c.send)
		c.conn.Close()
		c.hub.disconnect <- c
		if c.match != nil {
			c.match.end <- c
		}
	}()

	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error { c.conn.SetReadDeadline(time.Now().Add(pongWait)); return nil })

	for {
		msg := DataMessage{}
		err := c.conn.ReadJSON(&msg)
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("read error: %v", err)
			}
			break
		}

		switch msg.Name {
		case ConnectName:
			c.alias = msg.Data
			c.match = nil
			c.readyToPlay = true
			c.hub.connect <- c
		case PlayBotName:
			c.hub.bot <- c
		default:
			if c.isValidPlayMessage(&msg) && c.match != nil {
				c.match.play <- &PlayerMessage{player: c, message: &msg}
			}
		}
	}
}

func (c *Client) write() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()
	for {
		select {
		case msg := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			err := c.conn.WriteJSON(msg)
			if err != nil {
				log.Printf("write error: %v", err)
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				log.Printf("error: %v", err)
				return
			}
		}
	}
}

func (c *Client) isValidPlayMessage(msg *DataMessage) bool {
	if msg.Name == AttackName {
		return contains(c.attacks, msg.Data)
	}

	return contains(c.defenses, msg.Data)
}

func (c *Client) addAttack(a string) {
	if !contains(c.attacks, a) {
		c.attacks = append(c.attacks, a)
	}
}

func (c *Client) addDefense(d string) {
	if !contains(c.defenses, d) {
		c.defenses = append(c.defenses, d)
	}
}

func (c *Client) isReadyToPlay() bool {
	return c.readyToPlay
}

func (c *Client) sendOut(v interface{}) {
	c.send <- v
}

func (c *Client) getAttacks() []string {
	return c.attacks
}

func (c *Client) getDefenses() []string {
	return c.defenses
}

func (c *Client) setMatch(m *Match) {
	if m != nil {
		c.readyToPlay = false
	}

	c.match = m
}

func (c *Client) getAlias() string {
	return c.alias
}

func contains(a []string, s string) bool {
	for _, v := range a {
		if v == s {
			return true
		}
	}

	return false
}

func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	c := Client{conn: conn, send: make(chan interface{}), hub: hub}
	rand.Seed(time.Now().UnixNano())

	for len(c.attacks) < maxElements {
		r := rand.Intn(len(AttacksMap) - 1)
		if !contains(c.attacks, AttacksMap[r].Attack) {
			c.attacks = append(c.attacks, AttacksMap[r].Attack)
		}
	}

	for len(c.defenses) < maxElements {
		r := rand.Intn(len(AttacksMap) - 1)
		if !contains(c.defenses, AttacksMap[r].Defense) {
			c.defenses = append(c.defenses, AttacksMap[r].Defense)
		}
	}

	go c.write()
	go c.read()
}
