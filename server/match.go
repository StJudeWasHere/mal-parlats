package main

const (
	maxElements = 6
	maxAttacks  = 6
	maxScore    = 3
)

type Player interface {
	sendOut(v interface{})
	getAttacks() []string
	getDefenses() []string
	addAttack(a string)
	addDefense(b string)
	isReadyToPlay() bool
	setMatch(m *Match)
	getAlias() string
}

type Match struct {
	turn          Player
	oponent       Player
	currentAttack string
	isAttackTurn  bool
	play          chan *PlayerMessage
	end           chan Player
	active        bool
	attacksCount  int
	score         map[Player]int
}

func newMatch(p1 Player, p2 Player) {
	match := Match{
		turn:         p1,
		oponent:      p2,
		play:         make(chan *PlayerMessage),
		end:          make(chan Player),
		isAttackTurn: true,
		active:       true,
		score:        make(map[Player]int),
	}

	match.score[p1] = 0
	match.score[p2] = 0

	p1.setMatch(&match)
	p2.setMatch(&match)

	go match.Play()
}

func (m *Match) Play() {
	defer func() {
		m.turn.setMatch(nil)
		m.oponent.setMatch(nil)

		close(m.end)
		close(m.play)
	}()

	m.turn.sendOut(&StartMessage{
		Name:         StartName,
		Turn:         true,
		Attacks:      m.turn.getAttacks(),
		Defenses:     m.turn.getDefenses(),
		MaxAttacks:   maxAttacks,
		OponentAlias: m.oponent.getAlias(),
	})

	m.oponent.sendOut(&StartMessage{
		Name:         StartName,
		Turn:         false,
		Attacks:      m.oponent.getAttacks(),
		Defenses:     m.oponent.getDefenses(),
		MaxAttacks:   maxAttacks,
		OponentAlias: m.turn.getAlias(),
	})

	for m.active == true {
		select {
		case c := <-m.end:
			if c == m.turn {
				m.oponent.sendOut(&BasicMessage{Name: DisconnectName})
			} else {
				m.turn.sendOut(&BasicMessage{Name: DisconnectName})
			}
			m.active = false
		case msg := <-m.play:
			if msg.player != m.turn {
				return
			}

			if m.isAttackTurn {
				m.handleAttack(msg.message)
			} else {
				m.handleDefense(msg.message)
			}
		}
	}
}

func (m *Match) handleAttack(msg *DataMessage) {
	m.attacksCount++
	m.currentAttack = msg.Data
	m.isAttackTurn = false
	m.oponent.addAttack(msg.Data)
	m.oponent.sendOut(msg)
	m.turn, m.oponent = m.oponent, m.turn
}

func (m *Match) handleDefense(msg *DataMessage) {
	m.isAttackTurn = true
	m.oponent.sendOut(msg)

	v := isValidDefense(m.currentAttack, msg.Data)

	if v == false {
		m.score[m.oponent]++
		m.turn, m.oponent = m.oponent, m.turn
	}

	if m.score[m.turn] == maxScore || m.score[m.oponent] == maxScore || m.attacksCount == maxAttacks {
		m.active = false
		var turnData, oponentData string
		switch {
		case m.score[m.turn] == maxScore:
			turnData, oponentData = EndWin, EndLoose
		case m.score[m.oponent] == maxScore:
			turnData, oponentData = EndLoose, EndWin
		case m.attacksCount == maxAttacks:
			turnData, oponentData = EndTie, EndTie
		}

		m.turn.sendOut(&EndMessage{
			Name:         EndName,
			Data:         turnData,
			PlayerScore:  m.score[m.turn],
			OponentScore: m.score[m.oponent],
		})

		m.oponent.sendOut(&EndMessage{
			Name:         EndName,
			Data:         oponentData,
			PlayerScore:  m.score[m.oponent],
			OponentScore: m.score[m.turn],
		})

		return
	}

	m.turn.sendOut(&ValidationMessage{Name: ValidationName, Valid: v})
	m.oponent.sendOut(&ValidationMessage{Name: ValidationName, Valid: v})
	m.oponent.addDefense(msg.Data)
}
