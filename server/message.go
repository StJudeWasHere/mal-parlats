package main

const (
	DisconnectName = "disconnect"
	StartName      = "start"
	PlayName       = "play"
	AttackName     = "attack"
	DefenseName    = "defense"
	ConnectName    = "connect"
	ValidationName = "validation"
	PlayBotName    = "playbot"
	EndName        = "end"
	EndTie         = "tie"
	EndWin         = "win"
	EndLoose       = "loose"
)

type BasicMessage struct {
	Name string
}

type ValidationMessage struct {
	Name  string
	Valid bool
}

type DataMessage struct {
	Name string
	Data string
}

type EndMessage struct {
	Name         string
	Data         string
	PlayerScore  int
	OponentScore int
}

type StartMessage struct {
	Name         string
	Turn         bool
	Attacks      []string
	Defenses     []string
	MaxAttacks   int
	OponentAlias string
}

type PlayerMessage struct {
	player  Player
	message *DataMessage
}
