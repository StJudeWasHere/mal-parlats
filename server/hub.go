package main

type Hub struct {
	clients    map[Player]bool
	connect    chan Player
	disconnect chan Player
	bot        chan Player
}

func newHub() *Hub {
	return &Hub{
		connect:    make(chan Player),
		disconnect: make(chan Player),
		clients:    make(map[Player]bool),
		bot:        make(chan Player),
	}
}

func (h Hub) start() {
	for {
		select {
		case p1 := <-h.connect:
			h.clients[p1] = true
			for p2, _ := range h.clients {
				if p2 != p1 && p2.isReadyToPlay() {
					newMatch(p1, p2)
					break
				}
			}
		case p1 := <-h.bot:
			if p1.isReadyToPlay() {
				newMatch(p1, newBot())
			}
		case c := <-h.disconnect:
			delete(h.clients, c)
		}
	}
}
